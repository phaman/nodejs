const axios = require("axios");
const async = require("async");
const cheerio = require("cheerio");
const sequelize = (data_types = require("sequelize"));
const financial_report_model = require("./models/financial_report");
const company_model = require("./models/company");
const connection_string = "postgres://postgres:123@localhost/BCTC";
const _sequelize = new sequelize(connection_string, { logging: false });
const financial_report = financial_report_model(_sequelize, data_types);
const company = company_model(_sequelize, data_types);
var current_page = 1;

const first_url = "http://congbothongtin.ssc.gov.vn/idsPortal/thongtincongbo/index.ubck?kyBaoCao=3&rpage=100&cTDCThongTinCoSo=13655&cpage=" + current_page
var limit_page = 0;
var arr_financial_reports = [];


const read_financial_reports = async company_url => {
    try {
        const response = await axios.get(company_url);
        const $ = cheerio.load(response.data);
        $(".product-table .icon").remove();
        var arr_pages = $(".pages")
            .text()
            .split(" / ");
        limit_page = arr_pages[1];
        let arr_financial_reports = []
        var arr_columns = [];
        $(".product-table tr td").each((index, cell) => {
            var cell_text = $(cell)
                .text()
                .trim();
            // console.log(cell_text)
            arr_columns.push(cell_text);
            if (arr_columns.length == 2) {
                var params_string = $(cell)
                    .find("a")
                    .attr("href")
                    .replace(/([A-z])\w|(:)|(\()|(\))|(o)/g, "");
                var arr_parameters = params_string.split(", ");
                arr_columns.push(arr_parameters);
            }
            if (arr_columns.length == 7) {
                arr_financial_reports.push(arr_columns);
                arr_columns = [];
            }
        });
        var arr_next_page = await go_next_page();
        if (arr_next_page)
            arr_financial_reports = arr_financial_reports.concat(arr_next_page)
        return arr_financial_reports;
    } catch (error) {
        throw error;
    }
};
const insert_financial_report = async (company_id, row) => {
    try {
        arr_parameters = row[2];
        var financial_report_url =
            "http://congbothongtin.ssc.gov.vn/idsPortal/ttcb/bctc/chitietbaocao.ubck?bcbaocaoid=" +
            arr_parameters[0] +
            "&kyBaoCao=" +
            arr_parameters[1];
        var arr_period = row[4].split(" ");
        await financial_report.sync();

        var result = await financial_report.findOrCreate({
            where: {
                url: financial_report_url
            },
            defaults: {
                // set the default properties if it doesn't exist
                url: financial_report_url,
                status: "added",
                ref_table: arr_parameters[0] + "_" + arr_parameters[1],
                period: row[4],
                year: arr_period[2],
                company_id: company_id
            }
        });
        var found = result[0], // the instance of the company
            created = result[1]; // boolean stating if it was created or not
        if (!created)
            // false if company already exists and was not created.
            console.log("Financial report already exists with id:" + found.id);
        else console.log("Created financial report with id:" + found.id);
    } catch (error) {
        throw error
    }
};
const go_next_page = async () => {
    try {
        current_page++;
        const nextPageLink = "http://congbothongtin.ssc.gov.vn/idsPortal/thongtincongbo/index.ubck?kyBaoCao=3&rpage=100&cTDCThongTinCoSo=13655&cpage=" + current_page
        if (current_page > limit_page) {
            current_page = 1
            console.log("Done all");
        } else {
            var arr = await read_financial_reports(nextPageLink)
            return arr;
        };
    } catch (error) {
        throw error
    }
};
const crawl_financial_reports = async (company) => {
    try {
        var id = company.id;
        arr_financial_reports = await read_financial_reports(company.url)

        for (let row of arr_financial_reports) {
            try {
                await insert_financial_report(id, row)
            } catch (error) {
                console.log('error' + error);
            }
        }
        // asyncForEach(arr_financial_reports, async (row) => {
        //     await insert_financial_report(id, row)
        // }).catch((error) => { throw error })

    } catch (error) {
        throw error;
    }
};
//crawl_financial_reports({ id: 1, url: first_url });

function greek_number(g_n) {

    if (g_n == "I")
        return 1
    if (g_n == "II")
        return 2
    if (g_n == "III")
        return 3
    if (g_n == "IV")
        return 4
}
console.log(greek_number("II"))