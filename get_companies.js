const axios = require("axios");
const async = require('async');
const cheerio = require("cheerio");
const sequelize = data_types = require('sequelize')
const company_model = require('./models/company')
const connection_string = 'postgres://postgres:123@localhost/BCTC';
const _sequelize = new sequelize(connection_string, { logging: false });
const company = company_model(_sequelize, data_types)
company.sync();
var current_page = 1
var limit_page = 0
const first_url = "http://congbothongtin.ssc.gov.vn/idsPortal/ctdc/index.ubck?rpage=100&cpage=" + current_page
const get_company = (url) => {
    async.waterfall([
        function crawl_companies(cc_callback) {
            axios.get(url)
                .then((response) => {
                    const $ = cheerio.load(response.data)
                    var arr_pages = $('.pages').text().split(" / ");
                    limit_page = arr_pages[1];
                    var arr_companies = []
                    var arr_columns = []
                    $(".product-table tr td").each((index, cell) => {
                        var cell_text = $(cell).text().trim()
                        // console.log(cell_text)
                        arr_columns.push(cell_text)
                        if (arr_columns.length == 2) {
                            var a_href = "http://congbothongtin.ssc.gov.vn/idsPortal/thongtincongbo/index.ubck?nhomBcCt=1&kyBaoCao=3&rpage=100&cTDCThongTinCoSo=" + $(cell).find('a').attr('href').replace(/^\D+/g, '') + "&cpage="
                            // console.log(a_href)
                            arr_columns.push(a_href)
                        }
                        if (arr_columns.length == 8) {
                            arr_companies.push(arr_columns)
                            arr_columns = []
                        }
                    })
                    cc_callback(null, arr_companies)
                })
                .catch(ag_error => {
                    throw ag_error
                })
        },
        function insert_companies(arr_companies, ic_callback) {
            async.eachSeries(arr_companies, function (row, es_callback) {
                company.findOrCreate({
                    where: {
                        url: row[2]
                    },
                    defaults: {
                        name: row[1],
                        url: row[2],
                        stock_code: row[3],
                        post_to: row[4],
                        status: 'added'
                    }
                }).then((result) => {
                    var found = result[0], // the instance of the company
                        created = result[1]; // boolean stating if it was created or not
                    if (!created)  // false if company already exists and was not created.
                        console.log('Company already exists with id:' + found.id);
                    else
                        console.log('Created company with id:' + found.id);
                    es_callback();
                }).catch(foc_error => {
                    throw foc_error
                })
            }, function (es_error) {
                if (es_error)
                    throw es_error
                else
                    ic_callback(null, true);
            });
        }
    ], (wf_error) => {
        if (wf_error) {
            console.error(wf_error);
            return;
        }
        else {
            console.log('Done page ' + current_page);
            // Pagination Elements Link
            current_page++
            const next_page_url = "http://congbothongtin.ssc.gov.vn/idsPortal/ctdc/index.ubck?rpage=100&cpage=" + current_page
            if (current_page > limit_page) {
                console.log("Done all")
                current_page = 1
            }
            else
                get_company(next_page_url)
        }
    });
}
get_company(first_url)


