const axios = require("axios");
const cheerio = require("cheerio");
const async = require("async");
const Sequelize = require("sequelize");
const connectionString = "postgres://postgres:123@localhost/BCTC";
const sequelize = new Sequelize(connectionString, { logging: false });
const url_str =
  "http://congbothongtin.ssc.gov.vn/idsPortal/ttcb/bctc/chitietbaocao.ubck?bcbaocaoid=356080&kyBaoCao=3";

const element_to_text = (cherrio, element_string) => {
  return cherrio(element_string)
    .map(function() {
      return cherrio(this)
        .text()
        .trim();
    })
    .get();
};

const read_financial_report = async (url) => {
  try {
    const url_id = 1;
    console.log("Reading url id: " + url_id);
    const response = await axios.get(url);
    const $ = cheerio.load(response.data);
    var arr_info = element_to_text($, ".panel-info .form td");
    if (arr_info[3] === "BCTC riêng") {
      console.log("Skip");
      return;
    }
    arr_data = [];
    async.series(
      [
        function(callback) {
          var array = [];
          $("#BCDKT tr").each(function() {
            var x = $(this).children("td");
            if (x.length != 0) {
              array.push([
                $(x[0])
                  .text()
                  .trim(),
                $(x[1])
                  .text()
                  .trim(),
                $(x[2])
                  .text()
                  .trim(),
                $(x[3])
                  .text()
                  .trim(),
                $(x[4])
                  .text()
                  .trim()
              ]);
            }
          });
          callback(null, array);
        },
        function(callback) {
          var array = [];
          $("#KQKD tr").each(function() {
            var x = $(this).children("td");
            if (x.length != 0) {
              array.push([
                $(x[0])
                  .text()
                  .trim(),
                $(x[1])
                  .text()
                  .trim(),
                $(x[2])
                  .text()
                  .trim(),
                $(x[3])
                  .text()
                  .trim(),
                $(x[4])
                  .text()
                  .trim(),
                $(x[5])
                  .text()
                  .trim(),
                $(x[6])
                  .text()
                  .trim()
              ]);
            }
          });
          callback(null, array);
        },
        function(callback) {
          var array = [];
          $("#LCTT-TT tr").each(function() {
            var x = $(this).children("td");
            if (x.length != 0) {
              array.push([
                $(x[0])
                  .text()
                  .trim(),
                $(x[1])
                  .text()
                  .trim(),
                $(x[2])
                  .text()
                  .trim(),
                $(x[3])
                  .text()
                  .trim(),
                $(x[4])
                  .text()
                  .trim()
              ]);
            }
          });
          callback(null, array);
        },
        function(callback) {
          var array = [];
          $("#LCTT-GT tr").each(function() {
            var x = $(this).children("td");
            if (x.length != 0) {
              array.push([
                $(x[0])
                  .text()
                  .trim(),
                $(x[1])
                  .text()
                  .trim(),
                $(x[2])
                  .text()
                  .trim(),
                $(x[3])
                  .text()
                  .trim(),
                $(x[4])
                  .text()
                  .trim()
              ]);
            }
          });
          callback(null, array);
        }
      ],
      // optional callback
      function(s_error, results) {
        if (s_error) throw s_error;
        arr_data = results;
      }
    );
  } catch (rfr_error) {
    console.error(rfr_error);
  }
};

const x = [
  ["V. Tài sản ngắn hạn khác", "150", "35.370.726.849", "26.331.140.089"],
  ["1. Chi phí trả trước ngắn hạn", "151", "1.614.233.864", "443.234.596"],
  ["2. Thuế GTGT được khấu trừ", "152", "31.453.799.659", "24.129.338.389"],
  [
    "3. Thuế và các khoản khác phải thu Nhà nước",
    "153",
    "2.302.693.326",
    "1.758.567.104"
  ],
  ["4. Giao dịch mua bán lại trái phiếu Chính phủ", "154", "0", "0"],
  ["5. Tài sản ngắn hạn khác", "155", "0", "0"],
  ["B - TÀI SẢN DÀI HẠN", "200", "1.027.671.958.017", "939.504.623.313"],
  ["I. Các khoản phải thu dài hạn", "210", "2.994.779.076", "3.550.244.742"],
  ["1. Phải thu dài hạn của khách hàng", "211", "0", "0"],
  ["2. Trả trước cho người bán dài hạn", "212", "0", "0"],
  ["3. Vốn kinh doanh ở đơn vị trực thuộc", "213", "0", "0"],
  ["4. Phải thu nội bộ dài hạn", "214", "0", "0"],
  ["5. Phải thu về cho vay dài hạn", "215", "2.944.534.336", "3.500.000.002"],
  ["6. Phải thu dài hạn khác", "216", "50.244.740", "50.244.740"],
  ["7. Dự phòng phải thu dài hạn khó đòi (*)", "219", "0", "0"],
  ["II. Tài sản cố định", "220", "313.078.584.091", "315.304.833.923"],
  ["1. Tài sản cố định hữu hình", "221", "240.984.005.457", "242.576.553.974"],
  ["- Nguyên giá", "222", "589.624.160.985", "575.112.884.123"],
  [
    "- Giá trị hao mòn luỹ kế (*)",
    "223",
    "-348.640.155.528",
    "-332.536.330.149"
  ],
  ["2. Tài sản cố định thuê tài chính", "224", "0", "0"],
  ["- Nguyên giá", "225", "0", "0"],
  ["- Giá trị hao mòn luỹ kế (*)", "226", "0", "0"],
  ["3. Tài sản cố định vô hình", "227", "72.094.578.634", "72.728.279.949"],
  ["- Nguyên giá", "228", "93.176.416.886", "92.835.037.094"],
  ["- Giá trị hao mòn luỹ kế (*)", "229", "-21.081.838.252", "-20.106.757.145"],
  ["III. Bất động sản đầu tư", "230", "0", "0"],
  ["- Nguyên giá", "231", "0", "0"],
  ["- Giá trị hao mòn luỹ kế (*)", "232", "0", "0"],
  ["IV. Tài sản dở dang dài hạn", "240", "643.063.347.566", "547.705.487.912"],
  ["1. Chi phí sản xuất, kinh doanh dở dang dài hạn", "241", "0", "0"],
  [
    "2. Chi phí xây dựng cơ bản dở dang",
    "242",
    "643.063.347.566",
    "547.705.487.912"
  ]
];
const arr_data = [x];

const insert_database = async (id, ky) => {
  try {
    const BCDKTModel = require("./models/BCDKT");
    const KQKDModel = require("./models/KQKD");
    const LCTTTTModel = require("./models/LCTTTT");
    const LCTTGTModel = require("./models/LCTTGT");

    const BCDKT = BCDKTModel(sequelize, Sequelize, "BCDKT_" + id + "_" + ky);
    await BCDKT.sync();
    const KQKD = KQKDModel(sequelize, Sequelize, "KQKD_" + id + "_" + ky);
    await KQKD.sync();
    const LCTTTT = LCTTTTModel(sequelize, Sequelize, "LCTTTT_" + id + "_" + ky);
    await LCTTTT.sync();
    const LCTTGT = LCTTGTModel(sequelize, Sequelize, "LCTTGT_" + id + "_" + ky);
    await LCTTGT.sync();

    if (arr_data === []) throw new Error("Nothing");
    // async.series(
    //   [
    //     (callback) => {
    //       arr_data[0].forEach(async (row) => {
    //         await BCDKT.create({
    //           taisan: row[0],
    //           maso: row[1],
    //           thuyetminh: row[2],
    //           socuoiky: row[3],
    //           sodaunam: row[4]
    //         });
    //         console.log("BCDKT inserted");
    //       });
    //     }
    //   ],
    //   (error) => {
    //     if (error) console.error(error);
    //   }
    // );
    await asyncForEach(arr_data[0], async (row) => {
      await BCDKT.create({
        taisan: row[0],
        maso: row[1],
        thuyetminh: row[2],
        socuoiky: row[3],
        sodaunam: row[4]
      });
    });
    console.log("BCDKT inserted");
    await asyncForEach(arr_data[1], async (row) => {
      await KQKD.create({
        chitieu: row[0],
        maso: row[1],
        thuyetminh: row[2],
        kynamnay: row[3],
        kynamtruoc: row[4],
        luykekynamnay: row[5],
        luykekynamtruoc: row[6]
      });
    });
    console.log("KQKD inserted");
    await asyncForEach(arr_data[2], async (row) => {
      await LCTTTT.create({
        chitieu: row[0],
        maso: row[1],
        thuyetminh: row[2],
        luykekynamnay: row[3],
        luykekynamtruoc: row[4]
      });
    });
    console.log("LCTTTT inserted");
    await asyncForEach(arr_data[3], async (row) => {
      await LCTTGT.create({
        chitieu: row[0],
        maso: row[1],
        thuyetminh: row[2],
        luykekynamnay: row[3],
        luykekynamtruoc: row[4]
      });
    });
    console.log("LCTTGT inserted");
  } catch (error) {
    console.error(error);
  }
};
//insert_database("t", "t");

async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}

const update_status = async (is_done, url_id, id, ky) => {
  try {
    if (!is_done) throw new Error("Previous stage uncomplete! Can't update");
    const URL = URLModel(sequelize, Sequelize);
    await URL.update(
      {
        status: "done",
        refTable: id + "_" + ky
      },
      {
        where: {
          id: url_id
        }
      }
    );
  } catch (error) {
    throw error;
  }
};
