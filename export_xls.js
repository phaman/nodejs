if (typeof require !== "undefined") XLSX = require("xlsx");
const async = require("async");
/* create a new blank workbook */
var workbook = XLSX.utils.book_new();
var ws_name = "SheetJS";

const sequelize = data_types = require("sequelize");
const BCDKTModel = require("./models/BCDKT");
const KQKDModel = require("./models/KQKD");
const financial_reportModel = require("./models/financial_report");
const LCTTGTModel = require("./models/LCTTGT");
const company_model = require('./models/company')

const WeightModel = require("./models/WEIGHT");

const connectionString = "postgres://postgres:123@localhost/BCTC";
const _sequelize = new sequelize(connectionString, { logging: false });
const Op = sequelize.Op;

const BCDKTweight = WeightModel.BCDKT_weight(_sequelize, data_types);
const KQKDweight = WeightModel.KQKD_weight(_sequelize, data_types);
const LCTTGTweight = WeightModel.LCTTGT_weight(_sequelize, data_types);
const company = company_model(_sequelize, data_types)

const bcdkt_w = (BCDKT) => {
    return new Promise(async function (resolve, reject) {
        try {
            var bcdkt_weight = await BCDKTweight.findAll({
                attributes: ["id"],
                where: {
                    weight: { [Op.ne]: null }
                },
                order: [["weight", "ASC"]],
                raw: true
            });
            var x = map_to_string_array(bcdkt_weight);
            var bcdkt = await BCDKT.findAll({
                attributes: ["taisan", "socuoiky"],
                where: {
                    id: { [Op.in]: x }
                },
                raw: true
            });
            resolve(bcdkt);
        } catch (error) {
            reject(error);
        }
    });
};

const kqkd_w = (KQKD) => {
    return new Promise(async function (resolve, reject) {
        try {
            var kqkd_weight = await KQKDweight.findAll({
                attributes: ["id"],
                where: {
                    weight: { [Op.ne]: null }
                },
                order: [["weight", "ASC"]],
                raw: true
            });
            var x = map_to_string_array(kqkd_weight);
            var kqkd = await KQKD.findAll({
                attributes: ["chitieu", "kynamnay"],
                where: {
                    id: { [Op.in]: x }
                },
                raw: true
            });
            resolve(kqkd);
        } catch (error) {
            reject(error);
        }
    });
};

const lcttgt_w = (LCTTGT) => {
    return new Promise(async function (resolve, reject) {
        try {
            var lcttgt_weight = await LCTTGTweight.findAll({
                attributes: ["id"],
                where: {
                    weight: { [Op.ne]: null }
                },
                order: [["weight", "ASC"]],
                raw: true
            });
            var x = map_to_string_array(lcttgt_weight);
            var lcttgt = await LCTTGT.findAll({
                attributes: ["chitieu", "luykekynamnay"],
                where: {
                    id: { [Op.in]: x }
                },
                raw: true
            });
            resolve(lcttgt);
        } catch (error) {
            reject(error);
        }
    });
};

const add_data = async function (financial_report, is_header) {
    return new Promise(async function (resolve, reject) {
        try {
            const ref_table = financial_report.ref_table
            const period = greek_number(financial_report.period)

            const year = financial_report.year
            const company_id = financial_report.company_id
            const upload_time = financial_report.upload_time
            var is_consolidated = financial_report.is_consolidated ? "Báo cáo tổng hợp" : "Báo cáo riêng"
            var this_company = await company.findOne({
                where: { id: company_id },
                attributes: ["name", "stock_code", "post_to"],
                raw: true
            })
            var company_data = [this_company.name, this_company.stock_code, this_company.post_to]

            var arr_f_r = [period, year, upload_time, is_consolidated]
            const BCDKT = BCDKTModel(_sequelize, data_types, "BCDKT_" + ref_table);
            const LCTTGT = LCTTGTModel(_sequelize, data_types, "LCTTGT_" + ref_table);
            const KQKD = KQKDModel(_sequelize, data_types, "KQKD_" + ref_table);

            var bcdkt = await bcdkt_w(BCDKT);
            var bcdkt_data = normalize(bcdkt, "taisan", "socuoiky", is_header);
            var kqkd = await kqkd_w(KQKD);
            var kqkd_data = normalize(kqkd, "chitieu", "kynamnay", is_header);
            var lcttgt = await lcttgt_w(LCTTGT);
            var lcttgt_data = normalize(lcttgt, "chitieu", "luykekynamnay", is_header);
            var z = []
            if (is_header)
                z = [].concat(["period", "year", "upload_time", "type financial report"], ["name", "stock_code", "post_to"], bcdkt_data, kqkd_data, lcttgt_data);
            else
                z = [].concat(arr_f_r, company_data, bcdkt_data, kqkd_data, lcttgt_data);
            var ws_data = [].concat.apply([], z);
            resolve(ws_data);
        } catch (error) {
            reject(error);
        }
    });
};

const get_list_financial_reports = async () => {
    try {
        const financial_report = financial_reportModel(_sequelize, data_types);
        var financial_reports = await financial_report.findAll({
            attributes: ["ref_table", "period", "year", "upload_time", "is_consolidated", "company_id"],
            where: {
                status: "done",
                year: {
                    [Op.ne]: null
                }
            },
            raw: true
        });
        return financial_reports;
    } catch (error) {
        throw error;
    }
};
const exportexcel = async function () {
    try {
        var financial_reports = await get_list_financial_reports()
        var header = await add_data(financial_reports[1], true)
        var ws_header = [header];
        var ws_data = [];
        for (let financial_report of financial_reports) {
            try {
                var row = await add_data(financial_report, false)
                ws_data.push(row);

            } catch (error) {
                console.log('error' + error);
            }
        }
        write_xls(ws_header, ws_data)

    } catch (error) {
        console.log(error)
    }

};

exportexcel();

function map_to_string_array(weights) {
    return weights.map(function (weight) {
        return weight.id;
    });
}

function greek_number(g_n) {

    if (g_n == "I")
        return 1
    if (g_n == "II")
        return 2
    if (g_n == "III")
        return 3
    if (g_n == "IV")
        return 4
}

function normalize(data, param1, param2, is_header = true) {
    let result = [];
    let arr0 = [];
    let arr1 = [];
    for (let row of data) {
        for (let [key, value] of Object.entries(row)) {
            if (key === param1) {
                arr0.push(value);
            }
            if (key === param2) {
                arr1.push(value);
            }
        }
    }
    if (is_header) result = [arr0];
    else result = [arr1];

    return result;
}

function write_xls(ws_header, ws_data) {
    // console.log(ws_data);
    var ws = XLSX.utils.aoa_to_sheet(ws_header);

    /* Write data starting at A2 */
    //XLSX.utils.sheet_add_aoa(ws, ws_data, { origin: "A1" });
    /* Append row */
    XLSX.utils.sheet_add_aoa(ws, ws_data, { origin: -1 });

    /* Add the worksheet to the workbook */
    XLSX.utils.book_append_sheet(workbook, ws, ws_name);

    /* output format determined by filename */
    XLSX.writeFile(workbook, "out.xlsx");
}