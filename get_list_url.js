const axios = require("axios");
const async = require("async");
const cheerio = require("cheerio");
const Sequelize = require("sequelize");
const URLModel = require("./models/URL");
const connectionString = "postgres://postgres:123@localhost/BCTC";
const sequelize = new Sequelize(connectionString, { logging: false });
const URL = URLModel(sequelize, Sequelize);
var current_page = 1;
const url =
  "http://congbothongtin.ssc.gov.vn/idsPortal/ttcb/bctc/index.ubck?cpage=" +
  current_page +
  "&rpage=100&bmBaoCao=1_304&kyBaoCao=3";

const getWebsiteContent = async (url) => {
  try {
    const response = await axios.get(url);
    const $ = cheerio.load(response.data);
    var arr_pages = $(".pages")
      .text()
      .split(" / ");
    var limit_page = arr_pages[1];
    $(".product-table .icon").remove();
    // var urls_filtered = $(".product-table a").filter(function() {
    //   return $(this)
    //     .text()
    //     .includes("Báo cáo tài chính - Công ty đại chúng - Quý");
    // });
    async.eachSeries(
      $(".product-table a"),
      function(row, callback) {
        var input = $(row).attr("href");
        var cleantxt = input.replace(/([A-z])\w|(:)|(\()|(\))|(o)/g, "");
        var arr_parameters = cleantxt.split(", ");
        var url =
          "http://congbothongtin.ssc.gov.vn/idsPortal/ttcb/bctc/chitietbaocao.ubck?bcbaocaoid=" +
          arr_parameters[0] +
          "&kyBaoCao=" +
          arr_parameters[1];
        URL.sync().then(function() {
          URL.findOrCreate({
            where: {
              url: url
            },
            defaults: {
              // set the default properties if it doesn't exist
              url: url,
              status: "added",
              refTable: arr_parameters[0] + "_" + arr_parameters[1]
            }
          })
            .then(function(result) {
              var found = result[0], // the instance of the author
                created = result[1]; // boolean stating if it was created or not
              if (!created) {
                // false if url already exists and was not created.
                console.log("Url already exists with id:" + found.id);
              } else {
                console.log("Created url with id:" + found.id);
              }
              callback();
            })
            .catch((error) => {
              // Ooops, do some error-handling
              callback(error);
            });
        });
      },
      function(error) {
        if (error) {
          console.error(error);
        } else {
          console.log("Done page " + current_page);
          // Pagination Elements Link
          current_page++;
          const nextPageLink =
            "http://congbothongtin.ssc.gov.vn/idsPortal/ttcb/bctc/index.ubck?cpage=" +
            current_page +
            "&rpage=100&bmBaoCao=1_304&kyBaoCao=3";
          if (current_page > limit_page) {
            console.log("Done all");
          } else getWebsiteContent(nextPageLink);
        }
      }
    );
  } catch (error) {
    console.error(error);
  }
};
getWebsiteContent(url);
