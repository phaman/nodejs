const async = require('async');
const request = require("request");
const cheerio = require("cheerio");
const Sequelize = require('sequelize')
const BCDKTModel = require('./models/BCDKT')
const connectionString = 'postgres://postgres:123@localhost/BCTC';
const sequelize = new Sequelize(connectionString);

const url_str = 'http://congbothongtin.ssc.gov.vn/idsPortal/ttcb/bctc/chitietbaocao.ubck?bcbaocaoid=348600&kyBaoCao=2';
const current_url = new URL(url_str);
const search_params = current_url.searchParams;
const id = search_params.get('bcbaocaoid');
const ky = search_params.get('kyBaoCao');

const BCDKT = BCDKTModel(sequelize, Sequelize, id + '_' + ky)
BCDKT.sync({ force: true });
async.waterfall([
    function get_content(callback) {
        console.log('in get_content');
        request(url_str, function (error, response, html) {
            if (!error && response.statusCode == 200) {
                var $ = cheerio.load(html);
                var arr_data = [];
                var tr = $("#BCDKT tr");
                tr.each(function () {
                    var x = $(this).children('td');
                    if (x.length != 0) {
                        arr_data.push([$(x[0]).text(), $(x[1]).text(), $(x[3]).text(), $(x[4]).text()]);
                    }
                });
                callback(null, arr_data);
            }
            else {
                callback(error, null);
            }
        });
    },
    function insert_database(arr_data, callback) {
        if (arr_data == []) {
            console.log('nothing')
        }

        async.eachSeries(arr_data, function (row, callback) {
            var bcdkt = BCDKT.build({ taisan: row[0], maso: row[1], socuoiky: row[2], sodaunam: row[3] })
                .save()
                .then(() => {
                    // you can now access the currently saved task with the variable anotherTask... nice!
                    console.log(bcdkt.taisan)
                    console.log('success!')
                    callback();
                })
                .catch(error => {
                    callback(error);
                })

        }, function (err) {
            if (err) {
                console.log(err);
            } else {
                console.log('ok');
            }


        });
    }
], function (err, result) {
    if (err) {
        console.error(err);
        return;
    } else {
        console.log(result);
    }


});




