'use strict';

module.exports = function (sequelize, data_types) {
    const financial_report = sequelize.define('financial_report',
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: data_types.INTEGER
            },
            url: {
                type: data_types.STRING,
                //allowNull: false
                unique: true
            },
            status: {
                type: data_types.STRING
            },
            done_at: {
                type: data_types.DATE
            },
            ref_table: {
                type: data_types.STRING
            },
            period: {
                type: data_types.STRING
            },
            year: {
                type: data_types.STRING
            },
            upload_time: {
                type: data_types.STRING
            },
            is_consolidated: {
                type: data_types.BOOLEAN
            },
            company_id: {
                type: data_types.INTEGER,
                allowNull: false,
                references: {         // financial_report belongsTo company 1:1
                    model: 'companies',
                    key: 'id'
                }
            },
        },
        {
            // options
        });
    financial_report.associate = function (models) {
        financial_report.belongsTo(models.company, { foreignKey: 'company_id', as: 'company' })
    };
    return financial_report;
};