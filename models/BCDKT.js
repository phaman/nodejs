'use strict';

module.exports = function (sequelize, DataTypes, tableName) {
    const BCDKT = sequelize.define(tableName,
        {
            // attributes
            taisan: {
                type: DataTypes.STRING,
                allowNull: false
            },
            maso: {
                type: DataTypes.STRING
                // allowNull defaults to true
            },
            thuyetminh: {
                type: DataTypes.STRING
                // allowNull defaults to true
            },
            socuoiky: {
                type: DataTypes.STRING
                // allowNull defaults to true
            },
            sodaunam: {
                type: DataTypes.STRING
                // allowNull defaults to true
            }
        },
        {
            freezeTableName: true
        });
    return BCDKT;
};