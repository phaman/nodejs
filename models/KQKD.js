'use strict';

module.exports = function (sequelize, DataTypes, tableName) {
    const KQKD = sequelize.define(tableName,
        {
            // attributes
            chitieu: {
                type: DataTypes.STRING,
                allowNull: false
            },
            maso: {
                type: DataTypes.STRING
                // allowNull defaults to true
            },
            thuyetminh: {
                type: DataTypes.STRING
                // allowNull defaults to true
            },
            kynamnay: {
                type: DataTypes.STRING
                // allowNull defaults to true
            },
            kynamtruoc: {
                type: DataTypes.STRING
                // allowNull defaults to true
            },
            luykekynamnay: {
                type: DataTypes.STRING
                // allowNull defaults to true
            },
            luykekynamtruoc: {
                type: DataTypes.STRING
                // allowNull defaults to true
            }
        },
        {
            freezeTableName: true
        });
    return KQKD;
};