'use strict';

module.exports = {
    BCDKT_weight: function (sequelize, DataTypes) {
        const BCDKT_weight = sequelize.define('BCDKT_weight',
            {
                taisan: {
                    type: DataTypes.STRING,
                    //allowNull: false                   
                },
                weight: {
                    type: DataTypes.INTEGER
                }
            },
            {
                // options
            });

        return BCDKT_weight;
    },
    KQKD_weight: function (sequelize, DataTypes) {
        const KQKD_weight = sequelize.define('KQKD_weight',
            {
                chitieu: {
                    type: DataTypes.STRING,
                    //allowNull: false                  
                },
                weight: {
                    type: DataTypes.INTEGER
                }
            },
            {
                // options
            });
        return KQKD_weight;
    },
    LCTTGT_weight: function (sequelize, DataTypes) {
        const LCTTGT_weight = sequelize.define('LCTTGT_weight',
            {
                chitieu: {
                    type: DataTypes.STRING,
                    //allowNull: false                  
                },
                weight: {
                    type: DataTypes.INTEGER
                }
            },
            {
                // options
            });
        return LCTTGT_weight;
    }
} 