// producer.js
var zmq = require("zeromq"),
  sock = zmq.socket("push");
const async = require("async");
const Sequelize = require("sequelize");
const financial_reportModel = require("./models/financial_report");
const connectionString = "postgres://postgres:123@localhost/BCTC";
const sequelize = new Sequelize(connectionString, { logging: false });
const financial_report = financial_reportModel(sequelize, Sequelize);

sock.bindSync("tcp://127.0.0.1:3000");
console.log("Producer bound to port 3000");

var refreshId = setInterval(async function () {
  var f_r = await financial_report.findOne({
    where: { status: "added" },
    attributes: ["id", "url", "status", "year"]
  })
  if (f_r == null) {
    console.log("cant find");
    return;
  }
  if (f_r.year == null) {
    console.log("Skip!");
    await financial_report.update(
      { status: "skip" },
      { where: { id: f_r.id }, returning: true }
    )
    return;
  }
  if (!f_r.id) {
    console.log('false')
  }
  console.log("sending url");
  sock.send([f_r.id, f_r.url]);
  await financial_report.update(
    { status: "crawling" },
    { where: { id: f_r.id }, returning: true }
  )
  // clearInterval(refreshId);
}, 17000);
