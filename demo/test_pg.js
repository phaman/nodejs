const async = require('./node_modules/async');
const request = require("./node_modules/request");
const cheerio = require("cheerio");
var Client = require('./node_modules/pg').Client;
const connectionString = 'postgres://postgres:123@localhost/BCTC';
var client = new Client(connectionString);
client.connect();

var arr_data =
    [
        ['4 Tiền và các khoản tương đương tiền', '100', '10.255.270.683.954', '11.334.068.290.300'],
        ['4 Tiền ', '100', '10.255.270.683.954', '11.334.068.290.300']
    ];
async.each(arr_data,
    function insertData(item, callback) {
        client.query('INSERT INTO BCDKT_mau1(taisan, maso, socuoiky, sodaunam) VALUES($1, $2, $3, $4)',
            [
                item[0],
                item[1],
                item[2],
                item[3]
            ],
            function (err) {
                if (err) {
                    callback('Error: ' + err.message, null);

                }
                callback(null, 'success!');
            });

    },
    function (err, result) {
        if (err) {
            console.error(err);
            return;
        }
        client.end();
        console.log(result);

    });

