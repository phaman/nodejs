var Client = require('./node_modules/pg').Client;
var connectionString = 'postgres://postgres:123@localhost/BCTC';
var client = new Client(connectionString);
client.connect();
const createtableQuery = {
    text: 'CREATE TABLE BCDKT_mau1(stt SERIAL  PRIMARY KEY,taisan VARCHAR (100) UNIQUE NOT NULL,maso VARCHAR (6),socuoiky VARCHAR (100),sodaunam VARCHAR (100));',
}
const createtableQuery1 = {
    text: 'CREATE TABLE BCDKT_100() INHERITS (BCDKT_mau1);',
}
const inserttableQuery = {
    text: 'INSERT INTO BCDKT_mau1(taisan, maso, socuoiky, sodaunam) VALUES($1, $2, $3, $4) RETURNING *',
    value: ['I. Tiền và các khoản tương đương tiền', '100', '10.255.270.683.954', '11.334.068.290.300']
}
client.query(inserttableQuery.text, inserttableQuery.value, function (err, res) {
    if (err) {
        console.log('Error: ' + err.message);
        return;
    }
    console.log('Success!' + res.oid);
    client.end();
});
