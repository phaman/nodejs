const Sequelize = require("sequelize");
const async = require("async");
const connectionString = "postgres://postgres:123@localhost/BCTC";
const sequelize = new Sequelize(connectionString, { logging: false });
var id = "331840";
var ky = "3";
const BCDKTModel = require("./models/BCDKT");
const BCDKT = BCDKTModel(sequelize, Sequelize, "BCDKT_" + id + "_" + ky);
const Weight = require("./models/WEIGHT");
const BCDKTweight = Weight.BCDKT_weight(sequelize, Sequelize);

const set_weight = async () => {
  //await BCDKTweight.belongsTo(BCDKT, { targetKey: 'id', foreignKey: 'id' });
  await BCDKTweight.sync();
  var lcttgt = await BCDKT.findAll({ attributes: ["taisan"], raw: true });
  var count = 1;

  async.eachSeries(
    lcttgt,
    function (row, callback) {
      console.log(row.taisan);
      BCDKTweight.create({ taisan: row.taisan, weight: count })
        .then(() => {
          count++;
          callback();
        })
        .catch((error) => {
          throw error;
        });
    },
    function (err) {
      console.log("Hello");
      if (err) {
        console.error(err);
      } else {
        console.log("Finished processing all data");
      }
    }
  );
};

set_weight();

// sequelize
//     .authenticate()
//     .then(() => {
//         console.log('Connection has been established successfully.');
//     })
//     .catch(err => {
//         console.error('Unable to connect to the database:', err);
//     });
/*const BCTC = sequelize.define('BCTC',
    {
        // attributes
        taisan: {
            type: Sequelize.STRING,
            allowNull: false
        },
        maso: {
            type: Sequelize.STRING
            // allowNull defaults to true
        },
        socuoiky: {
            type: Sequelize.STRING
            // allowNull defaults to true
        },
        sodaunam: {
            type: Sequelize.STRING
            // allowNull defaults to true
        }
    },
    {
        // options
    });
//BCTC.sync()
var bctc = BCTC.build({ taisan: 'foo', maso: 'bar', socuoiky: '0000', sodaunam: '11111' })
    .save()
    .then(anotherTask => {
        // you can now access the currently saved task with the variable anotherTask... nice!
        console.log('success!')
    })
    .catch(error => {
        // Ooops, do some error-handling
        console.log(error)
    })
console.log(bctc.taisan)*/

// const URL = sequelize.define('URL',
//     {
//         // attributes
//         url: {
//             type: Sequelize.STRING,
//             allowNull: false,
//             unique: true
//         },
//         done: {
//             type: Sequelize.DATEONLY
//             // allowNull defaults to true
//         }
//     },
//     {
//         // options
//     });
// URL.sync()

// const { URL } = require('./sequelize');

// URL.findOne({
//     where: { id: '1' },
//     attributes: ['id', 'url', 'done']
// }).then(url => {
//     console.log(url.url);
// })

// const url = 'http://congbothongtin.ssc.gov.vn/idsPortal/ttcb/bctc/chitietbaocao.ubck?bcbaocaoid=348600&kyBaoCao=2';
// const current_url = new URL(url);
// const search_params = current_url.searchParams;

// // id = 123
// const id = search_params.get('bcbaocaoid');
// console.log(id);
// // type = article
// const ky = search_params.get('kyBaoCao');
// console.log(ky);
