// worker.js
var zmq = require("zeromq"),
    sock = zmq.socket("pull");
//var URL = require("url").URL;
const async = require("async");
const axios = require("axios");
const cheerio = require("cheerio");
const Sequelize = require("sequelize");
const BCDKTModel = require("./models/BCDKT");
const KQKDModel = require("./models/KQKD");
const LCTTTTModel = require("./models/LCTTTT");
const LCTTGTModel = require("./models/LCTTGT");
const financial_reportModel = require("./models/financial_report");
const connectionString = "postgres://postgres:123@localhost/BCTC";
const sequelize = new Sequelize(connectionString, { logging: false });

const element_to_text = (cherrio, element_string) => {
    return cherrio(element_string)
        .map(function () {
            return cherrio(this).text().trim();
        })
        .get();
};
const read_financial_report = async (fr_id, url, id, ky) => {
    try {
        const response = await axios.get(url);
        const $ = cheerio.load(response.data);
        var is_consolidated = true
        var arr_info = element_to_text($, ".panel-info .form td");
        if (arr_info[3] === "BCTC riêng" || arr_info[3] === "BCTC mẹ") {
            console.log("Unconsolidated");
            is_consolidated = false
        }
        arr_data = [];
        async.series(
            [
                function (callback) {
                    var array = [];
                    $("#BCDKT tr").each(function () {
                        var x = $(this).children("td");
                        if (x.length != 0) {
                            array.push([
                                $(x[0]).text().trim(),
                                $(x[1]).text().trim(),
                                $(x[2]).text().trim(),
                                $(x[3]).text().trim(),
                                $(x[4]).text().trim()
                            ]);
                        }
                    });
                    callback(null, array);
                },
                function (callback) {
                    var array = [];
                    $("#KQKD tr").each(function () {
                        var x = $(this).children("td");
                        if (x.length != 0) {
                            array.push([
                                $(x[0]).text().trim(),
                                $(x[1]).text().trim(),
                                $(x[2]).text().trim(),
                                $(x[3]).text().trim(),
                                $(x[4]).text().trim()
                            ]);
                        }
                    });
                    callback(null, array);
                },
                function (callback) {
                    var array = [];
                    $("#LCTT-TT tr").each(function () {
                        var x = $(this).children("td");
                        if (x.length != 0) {
                            array.push([
                                $(x[0]).text().trim(),
                                $(x[1]).text().trim(),
                                $(x[2]).text().trim(),
                                $(x[3]).text().trim(),
                                $(x[4]).text().trim(),
                                $(x[5]).text().trim(),
                                $(x[6]).text().trim()
                            ]);
                        }
                    });
                    callback(null, array);
                },
                function (callback) {
                    var array = [];
                    $("#LCTT-GT tr").each(function () {
                        var x = $(this).children("td");
                        if (x.length != 0) {
                            array.push([
                                $(x[0]).text().trim(),
                                $(x[1]).text().trim(),
                                $(x[2]).text().trim(),
                                $(x[3]).text().trim(),
                                $(x[4]).text().trim()
                            ]);
                        }
                    });
                    callback(null, array);
                }
            ],
            // optional callback
            function (s_error, results) {
                if (s_error) throw s_error;
                arr_data = results;
            }
        );
        return [arr_data, is_consolidated];
    } catch (rfr_error) {
        throw rfr_error;
    }
};
const insert_database = async (arr_data, id, ky) => {
    try {
        const BCDKT = BCDKTModel(sequelize, Sequelize, "BCDKT_" + id + "_" + ky);
        await BCDKT.sync();
        const KQKD = KQKDModel(sequelize, Sequelize, "KQKD_" + id + "_" + ky);
        await KQKD.sync();
        const LCTTTT = LCTTTTModel(sequelize, Sequelize, "LCTTTT_" + id + "_" + ky);
        await LCTTTT.sync();
        const LCTTGT = LCTTGTModel(sequelize, Sequelize, "LCTTGT_" + id + "_" + ky);
        await LCTTGT.sync();
        if (arr_data === []) throw new Error("Nothing");
        await asyncForEach(arr_data[0], async (row) => {
            await BCDKT.create({
                taisan: row[0],
                maso: row[1],
                thuyetminh: row[2],
                socuoiky: row[3],
                sodaunam: row[4]
            });
        });
        console.log("BCDKT inserted");
        await asyncForEach(arr_data[1], async (row) => {
            await KQKD.create({
                chitieu: row[0],
                maso: row[1],
                thuyetminh: row[2],
                kynamnay: row[3],
                kynamtruoc: row[4],
                luykekynamnay: row[5],
                luykekynamtruoc: row[6]
            });
        });
        console.log("KQKD inserted");
        await asyncForEach(arr_data[2], async (row) => {
            await LCTTTT.create({
                chitieu: row[0],
                maso: row[1],
                thuyetminh: row[2],
                luykekynamnay: row[3],
                luykekynamtruoc: row[4]
            });
        });
        console.log("LCTTTT inserted");
        await asyncForEach(arr_data[3], async (row) => {
            await LCTTGT.create({
                chitieu: row[0],
                maso: row[1],
                thuyetminh: row[2],
                luykekynamnay: row[3],
                luykekynamtruoc: row[4]
            });
        });
        console.log("LCTTGT inserted");
    } catch (error) {
        throw error;
    }
};
const update_status = async (url_id, id, ky, is_done, is_consolidated) => {
    try {
        var update_field = {}
        if (!is_done) {
            update_field = {
                status: "added",
            }
        }
        else {
            update_field = {
                status: "done",
                is_consolidated: is_consolidated,
                ref_table: id + "_" + ky
            }
        }
        const financial_report = financial_reportModel(sequelize, Sequelize);
        await financial_report.update(
            update_field,
            {
                where: {
                    id: url_id
                }
            }
        );
    } catch (error) {
        throw error;
    }
};

sock.connect("tcp://127.0.0.1:3000");
console.log("Worker connected to port 3000");

sock.on("message", async function (url_id, url) {
    var fr_id = Buffer.isBuffer(url_id) ? url_id.toString() : url_id


    const url_str = url.toString();
    // const url_str ="http://congbothongtin.ssc.gov.vn/idsPortal/ttcb/bctc/chitietbaocao.ubck?bcbaocaoid=356080&kyBaoCao=3";
    const current_url = new URL(url_str);
    const search_params = current_url.searchParams;
    const id = search_params.get("bcbaocaoid");
    const ky = search_params.get("kyBaoCao");
    try {
        console.log("Reading financial report with id: " + fr_id);
        var results = await read_financial_report(fr_id, url_str, id, ky)
        var arr_data = results[0]
        var is_consolidated = results[1]
        console.log("Done stage Reading!");
        await insert_database(arr_data, id, ky);
        console.log("Done stage Insert Database!");
        await update_status(fr_id, id, ky, true, is_consolidated)
        console.log("Financial Report updated!");
        console.log("Done id: " + fr_id);
    } catch (error) {
        if (error.response.status == 500) {
            await update_status(fr_id, id, ky, false, false)
            console.log('Rollback done!')
        }
        else {
            console.error(error)
        }

    }
});

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}
